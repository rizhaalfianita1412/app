import 'package:app_petpals/constants.dart';
import 'package:app_petpals/components/fav_page.dart';
import 'package:flutter/material.dart';

class AppFavorite extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Favorite',
      theme: ThemeData(
        scaffoldBackgroundColor: kBackgroundColor,
        primaryColor: kPrimaryColor,
      ),
      home: Favorite(),
    );
  }
}
