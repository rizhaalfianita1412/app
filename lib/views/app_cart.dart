import 'package:app_petpals/models/detail_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppCart extends StatefulWidget {
  @override
  _AppCartState createState() => _AppCartState();
}

class _AppCartState extends State<AppCart> {
  Color kPrimaryColor = Color.fromRGBO(246, 219, 195, 1);
  bool press = false;
  int harga = 0;

  Widget checkoutTemplate(dataCheckout) {
    String hargak = dataCheckout.price.replaceAll(".","").substring(3);

    int hargakucing = int.parse(hargak);
    return Card(
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.fromLTRB(25, 0, 25, 10),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: SizedBox(
                    height: 100,
                    width: 100,
                    child: Image.asset(
                      dataCheckout.imageAsset,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Transform.translate(
                  offset: Offset(0,-20),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            //color: Colors.red,
                            child: Text(
                              dataCheckout.name,
                              style: TextStyle(
                                  fontSize : 20,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: (){
                            setState(() {
                              if(press){
                                press =  false;
                                harga -= hargakucing;
                              }else{
                                press = true;
                                harga += hargakucing;
                              }
                            });
                          },
                          icon: (press? Icon(Icons.check_circle) : Icon(Icons.check_circle_outline)),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Container(
                    //color: Colors.green,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        dataCheckout.price,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: size.height * 0.4,
            decoration: BoxDecoration(
              color: kPrimaryColor,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(62),
                  bottomRight: Radius.circular(62)),
            ),
          ),
          Column(
            children: [
              Container(
                alignment: Alignment.center,
                height: size.height * 0.2,
                child: Text(
                  "Checkout",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 35,
                    fontFamily: "Poppins",
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: ListView(
                  children: detailDataList
                      .map((dataCheckout) => checkoutTemplate(dataCheckout))
                      .toList(),
                ),
              ),
              Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(25, 5, 25, 5),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                "Total",
                                style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontSize: 18,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  "$harga",
                                  style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      RaisedButton(
                        color: Color(0xf4b58302),
                        onPressed: () {},
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Buy",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                height: 100,
              ),
              Container(
                height: 42,
              )
            ],
          ),
        ],
      ),
    );
  }
}