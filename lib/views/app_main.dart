import 'package:app_petpals/views/app_home.dart';
import 'package:app_petpals/views/app_cart.dart';
import 'package:app_petpals/views/app_profile.dart';
import 'package:app_petpals/views/app_favorite.dart';
import 'package:app_petpals/constants.dart';
import 'package:flutter/material.dart';

class AppMain extends StatefulWidget {
  _AppMainState createState() => _AppMainState();
}

class _AppMainState extends State<AppMain> {
  int currentTab = 0;
  final List<Widget> screens = [
    AppHome(),
    AppCart(),
    AppFavorite(),
    AppProfile(),
  ];
  @override
  Widget currentScreen = AppHome();
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        body: PageStorage(
          child: currentScreen,
          bucket: PageStorageBucket(),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                MaterialButton(
                  onPressed: () {
                    setState(() {
                      currentScreen = AppHome();
                      currentTab = 0;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.home_filled,
                        color: (currentTab == 0 ? kPrimaryColor : Colors.grey),
                      ),
                    ],
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    setState(() {
                      currentScreen = AppCart();
                      currentTab = 1;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.shopping_cart,
                        color: (currentTab == 1 ? kPrimaryColor : Colors.grey),
                      ),
                    ],
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    setState(() {
                      currentScreen = AppFavorite();
                      currentTab = 2;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.favorite,
                        color: (currentTab == 2 ? kPrimaryColor : Colors.grey),
                      ),
                    ],
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    setState(() {
                      currentScreen = AppProfile();
                      currentTab = 3;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.person,
                        color: (currentTab == 3 ? kPrimaryColor : Colors.grey),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
