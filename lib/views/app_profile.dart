import 'package:app_petpals/constants.dart';
import 'package:flutter/material.dart';

class AppProfile extends StatelessWidget {
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Stack(
            children: [
              Container(
                  width: size.width,
                  height: size.height * 0.4 - 27,
                  decoration: BoxDecoration(
                    color: kPrimaryColor,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(75),
                      bottomRight: Radius.circular(75),
                    ),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(top: 60),
                    child: Text(
                      "Profile",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 35,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )),
              Container(
                width: size.width,
                height: size.height,
                margin: EdgeInsets.only(top: 200, left: 20, right: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(3, 3),
                      blurRadius: 50,
                      color: kTextColor.withOpacity(0.1),
                    ),
                  ],
                ),
                child: Container(
                  margin: EdgeInsets.only(top: 110),
                  child: Column(
                    children: [
                      Text(
                        "Leonardo de Caprio",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          color: kSecondTextColor,
                          fontSize: 23,
                        ),
                      ),
                      SizedBox(height: 7),
                      Text(
                        "timcakep@gmail.com",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          color: kSecondTextColor,
                          fontSize: 15,
                        ),
                      ),
                      SizedBox(height: 10),
                      Divider(
                        color: kTextColor.withOpacity(0.33),
                        indent: 0,
                      ),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            image: AssetImage("assets/icons/paw.png"),
                          ),
                          SizedBox(width: 5),
                          Text(
                            "2 Cats",
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: kSecondTextColor),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      PurchasedCatContainer(
                        cat_image: "assets/images/somali.jpg",
                        cat_name: "Somali",
                      ),
                      PurchasedCatContainer(
                        cat_image: "assets/images/bengals.jpg",
                        cat_name: "Bengals",
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 130),
                child: CircleAvatar(
                  backgroundImage: AssetImage("assets/images/decaprio.jpg"),
                  radius: 80,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PurchasedCatContainer extends StatelessWidget {
  const PurchasedCatContainer({
    Key? key,
    required this.cat_name,
    required this.cat_image,
  }) : super(key: key);

  final String cat_name, cat_image;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(kDefaultPadding),
      margin: EdgeInsets.only(bottom: 15),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Image.asset(
              cat_image,
              width: 100,
              height: 100,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: 15),
          Container(
            margin: EdgeInsets.only(bottom: 70),
            child: Text(
              "$cat_name",
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: kTextColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
