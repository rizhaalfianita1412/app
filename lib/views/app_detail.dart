import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:app_petpals/models/detail_data.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:math' as math;

class DetailScreen extends StatelessWidget{
  //final DetailData data;

  //const DetailScreen({Key? key, required this.data}) : super(key: key);

  final DetailData data;

  DetailScreen({required this.data});

  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Stack(
                children: [
                  Image.asset(data.imageAsset1),
                  SafeArea(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.arrow_back_ios_sharp),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        ),
                        IconButton(
                          icon: SvgPicture.asset('assets/icons/cart.svg'),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top:220),
                    padding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(57),
                          topLeft: Radius.circular(57),
                        )
                    ),
                  ),
              Container(
                  width: 330,
                  height: 260,
                  decoration: BoxDecoration(

                ),
                  child: Stack(
                children: <Widget>[
                  Positioned(
                      top: 198,
                      right: 4,
                      child: Container(
                          width: 63,
                          height: 61,
                          decoration: BoxDecoration(
                            boxShadow : [BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.15000000596046448),
                                offset: Offset(0,4),
                                blurRadius: 4
                            )],
                            color : Color.fromRGBO(255, 255, 255, 1),
                            borderRadius : BorderRadius.all(Radius.elliptical(63, 61)),
                          )
                      )
                  ),
                  Positioned(
                      top: 207,
                      right: 12,
                      child:
                  IconButton(
                  icon: SvgPicture.asset('assets/icons/favorite.svg'),
                    onPressed: (){
                    Navigator.pop(context);
                  },
                   ),

                  ),
                ]
            )
            ),
                  Container(
                      color: Colors.white,
                      margin: EdgeInsets.fromLTRB(23,235, 190, 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child:Column(
                                children: <Widget>[
                                  Text(
                                    data.name, textAlign: TextAlign.left, style: TextStyle(
                                      color: Color.fromRGBO(0, 0, 0, 1),
                                      fontFamily: 'Poppins',
                                      fontSize: 25,
                                      letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                      fontWeight: FontWeight.bold,
                                      height: 1
                                  ),
                                  ),
                                  SizedBox(height: 8.0),
                                  Text(
                                    "Rp " + data.price.toString(), textAlign: TextAlign.left, style: TextStyle(
                                      color: Color.fromRGBO(0, 0, 0, 1),
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                      fontWeight: FontWeight.w300,
                                      height: 1
                                  ),
                                  ),
                                ],
                              ),
                            ),
                          ]
                      )
                  ),
                ],
              ),



              Container(
                height: 150,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: 200,
                              height: 200,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                image: DecorationImage(
                                  image: AssetImage(data.imageAsset2),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(width: 8.0),
                            Container(
                              width: 200,
                              height: 200,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                image: DecorationImage(
                                  image: AssetImage(data.imageAsset3),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(width: 8.0),
                            Container(
                              width: 200,
                              height: 200,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                image: DecorationImage(
                                  image: AssetImage(data.imageAsset4),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                ),
                ),

              Container(
                margin: EdgeInsets.symmetric(vertical: 16.0),

                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(30, 5, 30, 5),
                        decoration: BoxDecoration(
                          color : Color.fromRGBO(255, 215, 177, 100),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                          child:
                            Column(

                              children: <Widget>[
                                Text(
                                    'Age',
                                  style: TextStyle(
                                      color: Color.fromRGBO(102, 73, 1, 1),
                                      fontFamily: 'Poppins',
                                      fontSize: 15,
                                      letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                      fontWeight: FontWeight.w900,
                                      height: 1
                                  ),
                                ),
                                SizedBox(height: 8.0),
                                Text(
                                  data.age.toString(),
                                )
                              ],
                            ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(30, 5, 30, 5),
                        decoration: BoxDecoration(
                          color : Color.fromRGBO(255, 215, 177, 100),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child:
                        Column(
                          children: <Widget>[
                            Text(
                                'Weight',
                              style: TextStyle(
                                  color: Color.fromRGBO(102, 73, 1, 1),
                                  fontFamily: 'Poppins',
                                  fontSize: 15,
                                  letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                  fontWeight: FontWeight.w900,
                                  height: 1
                              ),
                            ),
                            SizedBox(height: 8.0),
                            Text(
                                data.weight.toString() + " kg",
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(30, 5, 30, 5),
                        decoration: BoxDecoration(
                          color : Color.fromRGBO(255, 215, 177, 100),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child:
                        Column(
                          children: <Widget>[
                            Text(
                                'Sex',
                              style: TextStyle(
                                  color: Color.fromRGBO(102, 73, 1, 1),
                                  fontFamily: 'Poppins',
                                  fontSize: 15,
                                  letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                  fontWeight: FontWeight.w900,
                                  height: 1
                              ),
                            ),
                            SizedBox(height: 8.0),
                            Text(
                              data.sex,
                            )
                          ],
                        ),
                      ),
                    ]
                ),
              ),

              Container(
                  margin: EdgeInsets.fromLTRB(14, 4, 14, 4),
                  padding: EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                    color : Color.fromRGBO(255, 215, 177, 100),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Text(
                    data.about,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color.fromRGBO(102, 73, 1, 1),
                        fontFamily: 'Poppins',
                        fontSize: 14,
                        letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                        fontWeight: FontWeight.normal,
                        height: 1
                    ),
                  )
              ),
              Container(
                margin: EdgeInsets.fromLTRB(14, 13, 14, 13),
                width: 100,
                height: 50,
                child: Stack(
                    children: <Widget>[
                      Positioned(
                      width:330,
                          height: 40,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              backgroundColor: Color.fromRGBO(182, 131, 2, 30),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            onPressed: () {},
                            child: Text(
                              "Adopt",
                              style:TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontFamily: 'Montserrat',
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                      ),
                      Positioned(
                        top:3,
                        left: 15,
                        child:
                        Transform.rotate(
                          angle: -math.pi / 6,
                          child:  Image.asset('images/icons/Dogpaw.png',
                          color: Colors.white,
                          width: 33,
                          height: 36,
                          ),
                        )

                        ),
                    ]
                )
                ),


            ],
          )
      ),
    );
  }
}

class CartButton extends StatefulWidget{
  _CartButtonState createState() => _CartButtonState();
}

class _CartButtonState extends State<CartButton>{
  bool isCart = false;

  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        isCart ? Icons.shopping_cart : Icons.shopping_cart,
        color: Colors.black,
      ),
      onPressed: (){
        setState(){
          isCart = !isCart;
        }
      },

    );
  }
}

class FavButton extends StatefulWidget{
  _FavButtonState createState() => _FavButtonState();
}

class _FavButtonState extends State<FavButton>{
  bool isCart = false;

  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        isCart ? Icons.shopping_cart : Icons.shopping_cart,
        color: Colors.black,
      ),
      onPressed: (){
        setState(){
          isCart = !isCart;
        }
      },

    );
  }
}