import 'dart:async';
import 'package:app_petpals/views/app_login_1.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenWidgetState createState() => _SplashScreenWidgetState();
}

class _SplashScreenWidgetState extends State<SplashScreen> {
  @override
  void initState(){
    super.initState();
    Timer(Duration(seconds: 5),(){
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_)=>AppLoginFirst()));
    }
    );
  }

  @override
  Widget build(BuildContext context) {
    // Figma Flutter Generator SplashscreenWidget - FRAME

    return Scaffold(
        backgroundColor: Color.fromRGBO(255, 215, 177, 65),
        body: Stack(
            children: <Widget>[
              Container(
                  child: Stack(
                      children: <Widget>[

                        Positioned(
                            top:230,
                            right: 25,
                            child:
                            Transform.rotate(
                              angle: math.pi / 35,
                              child:  Image.asset('assets/images/icondetail/bulet2.png',
                              ),
                            )
                        ),
                        Positioned(
                            top:230,
                            left: 35,
                            child:
                            Transform.rotate(
                              angle: math.pi / 9,
                              child: Image.asset('assets/images/icondetail/bulet1.png',
                              ),
                            )
                        ),

                        Positioned(
                            top:310,
                            right: 80,
                            child:
                            Transform.rotate(
                              angle: math.pi / 6,
                              child:  Image.asset('assets/images/icondetail/Dogpaw.png',
                                color: Color.fromRGBO(255, 215, 177, 100),
                                width: 33,
                                height: 36,
                              ),
                            )
                        ),
                        Positioned(
                            top:346,
                            right:95,
                            child:
                            Transform.rotate(
                              angle: math.pi / 4,
                              child:  Image.asset('assets/images/icondetail/Dogpaw.png',
                                color: Color.fromRGBO(255, 215, 177, 100),
                                width: 33,
                                height: 36,
                              ),
                            )

                        ),
                        Positioned(
                            top:342,
                            right: 60,
                            child:
                            Transform.rotate(
                              angle: -math.pi / 7,
                              child:  Image.asset('assets/images/icondetail/Dogpaw.png',
                                color: Color.fromRGBO(255, 215, 177, 100),
                                width: 33,
                                height: 36,
                              ),
                            )

                        ),
                        Positioned(
                          top:350,
                          left: 60,
                          child: Text(
                            "Pet",
                            style: TextStyle(
                                color: Color.fromRGBO(182, 131, 2, 96),
                                fontFamily: 'Poppins',
                                fontSize: 48,
                                letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.w900,
                                height: 1
                            ),
                          ),
                        ),
                        Positioned(
                          top:350,
                          left: 135,
                          child: Text(
                            "Pals",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Poppins',
                                fontSize: 48,
                                letterSpacing: 0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.w900,
                                height: 1
                            ),
                          ),
                        ),
                      ]
                  )
              ),
                      ]
                  )
    );
  }
}
