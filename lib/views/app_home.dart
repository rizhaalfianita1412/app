import 'package:app_petpals/components/body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppHome extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: Body(),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
    );
  }
}
