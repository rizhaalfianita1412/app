import 'package:app_petpals/views/app_login.dart';
import 'package:app_petpals/views/app_register.dart';
import 'package:flutter/material.dart';

class AppLoginFirst extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color.fromRGBO(246, 219, 195, 1),
        body: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //BLOB KANAN
                    Container(
                      child: Row(
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            child: Image.asset(
                              'assets/images/blobkiri1.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                    ),

                    //TULISAN PETPALS
                    Container(
                      child: Row(
                        children: [
                          Text(
                            'Pet',
                            style: TextStyle(
                                fontSize: 36,
                                fontFamily: 'Poppins',
                                color: Color.fromRGBO(182, 131, 2, 1)),
                          ),
                          Text(
                            'Pals',
                            style: TextStyle(
                                fontSize: 36,
                                fontFamily: 'Poppins',
                                color: Colors.white),
                          ),
                          Container(
                            height: 30,
                            width: 30,
                            child: Transform.rotate(
                              angle: 0.54,
                              child: Image.asset(
                                'assets/images/Dogpaw.png',
                                fit: BoxFit.cover,
                                color: Color.fromRGBO(255, 238, 243, 1.0),
                              ),
                            ),
                          ),
                          //BLOB KANAN
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  height: 100,
                                  width: 100,
                                  child: Image.asset(
                                    'assets/images/blobkanan1.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              Text(
                "Find your lovely cat now!",
                style: TextStyle(
                    fontSize: 13,
                    fontFamily: 'Poppins',
                    color: Color.fromRGBO(72, 37, 3, 1)),
              ),

              //BUTTON
              SizedBox(
                height: 120,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return AppLogin(); //DIGANTI
                  }));
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(86, 14, 86, 14),
                  child: Text(
                    "Login",
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Montserrat',
                        color: Color.fromRGBO(182, 131, 2, 1)),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return AppRegister(); //DIGANTI
                  }));
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                color: Color.fromRGBO(182, 131, 2, 1),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(81, 14, 81, 14),
                  child: Text(
                    "Sign Up",
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: 'Montserrat',
                        color: Colors.white),
                  ),
                ),
              ),

              SizedBox(
                height: 38,
              ),
              Image.asset(
                'assets/images/catt.jpeg',
                width: 300,
                height: 310,
              )
            ],
          ),
        ),
      ),
    );
  }
}
