import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFFFD7B1);
const kTextColor = Color(0xFF3C4046);
const kBackgroundColor = Color(0xFFF9F8FD);
const kSecondTextColor = Color(0xFFB9880C);

const double kDefaultPadding = 10.0;
