import 'package:app_petpals/views/app_splash_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Main());
}

class Main extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: null,
      home: SplashScreen(),
    );
  }
}