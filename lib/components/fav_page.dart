import 'package:app_petpals/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Favorite extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          Container(
            height: size.height,
            child: Stack(
              children: <Widget>[
                Container(
                  height: size.height * 0.4,
                  decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(62),
                        bottomRight: Radius.circular(62),
                        ),
                      ),
                  child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      padding:EdgeInsets.only(
                          top: 90
                      ),
                      alignment: Alignment.topCenter,
                      child: Text(
                            'Favorite',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 36.0,
                              fontFamily: 'Poppins',
                              color: kTextColor,
                            ),
                          ),
                      ),
                ),
                Container(
                          height: 1200,
                          padding: EdgeInsets.only(top: 230),
                          child: GridView.count(
                            crossAxisCount: 2,
                            crossAxisSpacing: 24,
                            mainAxisSpacing: 24,
                            physics: NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            children: <Widget>[
                              CatCard(
                                catname: 'Ragdoll',
                                catpic: 'assets/images/ragdoll.jpg',
                              ),
                              CatCard(
                                  catname: 'Bengals',
                                  catpic: 'assets/images/bengals.jpg'
                              ),
                              CatCard(
                                  catname: 'Siamese',
                                  catpic: 'assets/images/siamese.jpg'
                              ),
                              CatCard(
                                  catname: 'Bengals',
                                  catpic: 'assets/images/bengals.jpg'
                              ),
                              CatCard(
                                  catname: 'Pikachu',
                                  catpic: 'assets/images/pikachu_cat.png'
                              ),
                              CatCard(
                                  catname: 'Ragdoll',
                                  catpic: 'assets/images/ragdoll.jpg'
                              ),
                            ],

                        ),
                      ),

              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CatCard extends StatelessWidget {
  final String catname;
  final String catpic;
  const CatCard({
    Key? key,
    required this.catname,
    required this.catpic,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              offset: Offset(0, 3),
              blurRadius: 4
          )
        ],
        borderRadius: BorderRadius.circular(12),
        image: DecorationImage(
          image: AssetImage(catpic),
          fit: BoxFit.cover,
        ),
      ),

          child: Transform.translate(
            offset: Offset(8, 110),
            child: Container(
              height: 100,
              child: Text(
                catname,
                textAlign: TextAlign.left,
                style: TextStyle(
                  shadows: [
                    Shadow(
                      color: Colors.grey,
                      offset: Offset(0, 2),
                      blurRadius: 4,
                    )
                  ],
                  color: Colors.white,
                  fontSize: 18.0,
                  fontFamily: 'Montserrat',
                ),
    ),
            ),
          ),

    );
  }
}

final List<String> cats = [
  'assets/images/ragdoll.jpg',
  'assets/images/bengals.jpg',
  'assets/images/siamese.jpg',
  'assets/images/bengals.jpg',
  'assets/images/pikachu_cat.png',
  'assets/images/ragdoll.jpg'];