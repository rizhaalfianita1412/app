import 'package:app_petpals/constants.dart';
import 'package:app_petpals/models/detail_data.dart';
import 'package:app_petpals/views/app_detail.dart';
import 'package:flutter/material.dart';
import 'header_with_searchbox.dart';

class Body extends StatelessWidget {
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          HeaderWithSearchBox(size: size),
          Container(
            height: 1200,
            child: GridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 5,
              childAspectRatio: 3 / 4,
              physics: NeverScrollableScrollPhysics(),
              children: detailDataList.map((data){
                return Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) {
                        return DetailScreen(data: data);
                      }));
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 30, left: 10, right: 10),
                      width: size.width * 0.4,
                      height: 200,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 10),
                            blurRadius: 56,
                            color: kPrimaryColor.withOpacity(0.23),
                          ),
                        ],
                        borderRadius: BorderRadius.circular(15),
                        image: DecorationImage(
                          image: AssetImage(data.imageAsset),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          gradient: LinearGradient(
                            begin: Alignment.bottomRight,
                            colors: [
                              Color.fromRGBO(255, 215, 177, 0.4),
                              Color.fromRGBO(255, 215, 177, 0.1),
                            ],
                          ),
                        ),
                        child: Transform.translate(
                          offset: Offset(0, 60),
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 50),
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            height: 60,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white,
                            ),
                            child: Row(
                              children: <Widget>[
                                RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                        text: data.name+"\n\n",
                                        style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 17,
                                            fontWeight: FontWeight.w600,
                                            color: kTextColor),
                                      ),
                                      TextSpan(
                                        text: "Start in\n",
                                        style: TextStyle(
                                            fontFamily: 'Poppins',
                                            color: kTextColor.withOpacity(0.5),
                                            fontSize: 10),
                                      ),
                                      TextSpan(
                                        text: data.price,
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold,
                                          color: kTextColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  margin: EdgeInsets.only(top: 50),
                                  child: Icon(
                                    Icons.favorite,
                                    color: Colors.red,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }).toList()
            ),
          )
        ],
      ),
    );
  }
}