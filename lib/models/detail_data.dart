class DetailData {
  String name;
  String price;
  String age;
  String weight;
  String sex;
  String about;
  String imageAsset;
  String imageAsset1;
  String imageAsset2;
  String imageAsset3;
  String imageAsset4;

  DetailData({
    required this.name,
    required this.price,
    required this.age,
    required this.weight,
    required this.sex,
    required this.about,
    required this.imageAsset,
    required this.imageAsset1,
    required this.imageAsset2,
    required this.imageAsset3,
    required this.imageAsset4,
  });
}

var detailDataList = [
  DetailData(
    name: 'Ragdoll',
    price : 'Rp 4.500.000',
    age: '4',
    weight: '6',
    sex: 'Male',
    about: 'Kucing Ragdoll pertama kali dikembangbiakkan pada 1963 oleh seorang warga asal California, '
        '   Amerika Serikat bernama Ann Baker yang merupakan seorang peternak kucing. '
        'Ras kucing ini sendiri didapatkan melalui persilangan beberapa gen kucing, '
        'seperti kucing Birman, kucing Persia, dan kucing Burmese.Ragdoll baru merdaftar sebagai ras '
        'kucing resmi oleh Cat Fancier Association (CFA) pada 1993 dan meraih penghargaan pada tahun 2000 lalu.',
    imageAsset: 'assets/images/ragdoll.jpg',
    imageAsset1: 'assets/images/1ragdoll/ragdol1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Maine Coons',
    price : 'Rp 1.500.000',
    age: '4',
    weight: '5',
    sex: 'Male',
    about: 'SSejarah kucing maine coon sesuai dengan namanya mereka berasal dari kota Maine, disana '
        'pada kala itu sangat terkenal sebagai tempat yang mana populasi dari tikusnya sangatlah tinggi, '
        'oleh karena itu kehadiran trah ini seringkali dijadikan sebagai kucing pertanian dan juga kucing '
        'kapal yang bertugas membasmi tikus yang sangat banyak hingga abad ke-19.',
    imageAsset: 'assets/images/maine_coons.jpg',
    imageAsset1: 'assets/images/2mainecoons/main1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Siamese',
    price : 'Rp 2.300.000',
    age: '5',
    weight: '7',
    sex: 'Male',
    about: 'Sejarah dari Kucing siamese dimulai di amerika serikat pada tahun 1877 hingga 1881 ketika '
        'presiden Rutherford B. Hayes dan istrinya yang bernama lucy mendapatkan hadian berupa kucing ras '
        'siamese dari seseorang yang bernama David B Sickels yang merupakan seorang diplomat AS yang '
        'ditempatkan di Thailand.',
    imageAsset: 'assets/images/siamese.jpg',
    imageAsset1: 'assets/images/3siamese/siam1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Bengals',
    price : 'Rp 40.000.000',
    age: '3',
    weight: '4',
    sex: 'Male',
    about: 'Nama Kucing bengal sendiri diambil dari istilah ilmiah macan tutul asia yang bernama Felis bengalensis. '
        'Mereka diciptakan dengan melakukan persilangan antara macan tutul asia yang ada pada tahun 1950an sampai '
        '1960an yang pada saat itu bisa dibeli di toko hewan sebagai hewan peliharaan.',
    imageAsset: 'assets/images/bengals.jpg',
    imageAsset1: 'assets/images/4bengal/bengal1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Sphynx',
    price : 'Rp 25.000.000',
    age: '2',
    weight: '6',
    sex: 'Male',
    about: 'Kucing Sphynx merupakan kucing yang sebenarnya tidak diharapkan untuk ada, mereka hadir '
        'berawal dari ketidak sengajaan.pada saat itu ada seseorang yang bernama elizabeth berasal dari '
        'toronto melakukan perkawinan terhadap kucingnya yang bernama prune, pada saat itu prune di silangkan '
        'dengan ras domestik berwarna dan hitam, hasil dari persilangannya menghasilkan dua anak kucing yang '
        'ternyata terdapat kelainan genetik.',
    imageAsset: 'assets/images/sphynx.jpg',
    imageAsset1: 'assets/images/5sphynx/sphynx1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Scottish Fold',
    price : 'Rp 25.000.000',
    age: '4',
    weight: '4',
    sex: 'Female',
    about: 'Asal mula kucing ini berasal dari seekor kucing putih yang memiliki bentuk telinga unik yang '
        'terlipat ke depan. Kucing ini bernama Susie yang ditemukan di peternakan sekitar Coupar Angus di '
        'Perthshire, Skotlandia, pada tahun 1961. Hingga Susie akhirnya memiliki anak dengan dua di antaranya '
        'mewarisi bentuk telinganya. Satu anak betina Susie yang memiliki telinga lipat diadopsi oleh seorang '
        'pecinta kucing bernama William Rose.',
    imageAsset: 'assets/images/scottish.jpg',
    imageAsset1: 'assets/images/6scottish/scottish1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Somali',
    price : 'Rp 21.000.000',
    age: '5',
    weight: '4',
    sex: 'Male',
    about: ' Kucing Somali berasal dari daerah pantai Samudra Hindia dan Asia Tenggara serta merupakan '
        'hasil perkawinan silang antara kucing Abyssinian dengan jenis kucing berbulu panjang.Perkawinan silang '
        'ini terjadi karena pada tahun 1950-an terdapat program pemuliaan kelahiran kucing berbulu panjang. '
        'Namun, alih-alih menjadi primadona, gen resesif yang dimiliki oleh kucing Somali justru sempat dianggap '
        'tidak menarik oleh kalangan peternak.',
    imageAsset: 'assets/images/somali.jpg',
    imageAsset1: 'assets/images/7somali/somali1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Persian',
    price : 'Rp 16.000.000',
    age: '5',
    weight: '6',
    sex: 'Female',
    about: 'Lahirnya kucing ras ini didapat dari kawin silang antara kucing berbulu putih dan kucing berbulu '
        'hitam. Para penggemar kucing lalu terus melakukan pengembangbiakan pada ras kucing ini. Jadi nggak '
        'heran, jika sekarang Blibli Friends bisa menemukan kucing Persia dengan berbagai macam warna bulu '
        'yang mempesona.',
    imageAsset: 'assets/images/persian.jpg',
    imageAsset1: 'assets/images/8persian/persian1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Abyssinian',
    price : 'Rp 32.200.000',
    age: '6',
    weight: '7',
    sex: 'Male',
    about: 'Meskipun asal muasal kucing yang menarik ini tidak diketahui, mereka telah berada di Inggris '
        'selama lebih dari 100 tahun dan didokumentasikan dalam Standar Poin pertama yang diterbitkan oleh '
        'Harrison Weir pada tahun 1889, dan oleh Frances Simpson dalam ‘The Book of the Cat‘ , diterbitkan '
        'pada tahun 1903. Hal ini menjadikan Abyssinian salah satu ras kucing paling tua.',
    imageAsset: 'assets/images/abyss.jpg',
    imageAsset1: 'assets/images/9abyssinian/abyssinian1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
  DetailData(
    name: 'Birman',
    price : 'Rp 12.000.000',
    age: '4',
    weight: '5',
    sex: 'Female',
    about: 'Secara singkat kucing Birman adalah sahabat biksu atau pendeta di Myanmar. Kucing ini berwarna '
        'putih dan diberi nama Sinh. Namun, sang pendeta meninggal dan sang kucing melompat di atas tubuhnya. '
        'Bulunya yang putih kemudian terdapat warna kuning yang mirip dengan dewi kuil. Adanya warna coklat gelap '
        'di daerah kuping, hidung, ekor dan kakinya menandakan warna bumi. Cakar atau tekapak kaki berwarna putih '
        'yang melambangkan kesucian.',
    imageAsset: 'assets/images/birman.jpg',
    imageAsset1: 'assets/images/10birman/birman1.jpg',
    imageAsset2: 'assets/images/1ragdoll/ragdol2.jpg',
    imageAsset3: 'assets/images/1ragdoll/ragdol3.jpg',
    imageAsset4: 'assets/images/1ragdoll/ragdol4.jpg',
  ),
];