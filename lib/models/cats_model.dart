class Cat {
  final int id;
  final String name;
  final String imagePath;
  final int price;

  Cat({
    required this.id,
    required this.name,
    required this.imagePath,
    required this.price,
  });
}
